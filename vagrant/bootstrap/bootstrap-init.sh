#!/usr/bin/env bash

##### Configure #####

# Create directories
if [ ! -d /home/vagrant/logs ]; then
  mkdir /home/vagrant/logs
  chown vagrant:vagrant /home/vagrant/logs
fi

# Replace nginx configuration file
if [ -f /etc/apache2/sites-enabled/000-default.conf ]; then
  rm /etc/apache2/sites-enabled/000-default.conf
fi
ln -sfn /vagrant/bootstrap/config/apache.conf /etc/apache2/sites-enabled/app.conf
service apache2 restart

# Replace apache User and Group
sed -i 's/export APACHE_RUN_USER=.*/export APACHE_RUN_USER=vagrant/g' /etc/apache2/envvars
sed -i 's/export APACHE_RUN_GROUP=.*/export APACHE_RUN_GROUP=vagrant/g' /etc/apache2/envvars
service apache2 restart