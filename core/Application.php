<?php

namespace core;

use Exception;

/**
 * Базовый класс приложений.
 * Инициализирует и выступает в роли контейнера основынх компонентов.
 * 
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 * @package core
 * 
 * @property \components\LoggerInterface $logger {@link Appliction::__get()}.
 */
class Application
{

    /**
     * @var array
     */
    private $_components= [];

    /**
     * Контейнер для инициализированных компонентов.
     * @var array
     */
    private $_instantiatedComponents = [];

    /** array */
    private $_params = [];

    /**
     * Конструктор.
     * @param array $config данные для конфигурирования.
     */
    public function __construct($config = null)
    {
        $this->_components = $config['components'];
        $this->_params = isset($config['params']) ? $config['params'] : $this->_params;
    }

    /**
     * Параметры из секции конфига "params".
     * @return array
     */
    public function getParams()
    {
        return $this->_params;
    }

    /**
     * Возвращает запрашиваемый компонент.
     * @param string $name наименование компонента.
     * @return mixed
     * @throws Exception если компонент не найден.
     */
    public function __get($name)
    {
        if (isset($this->_components[$name])) {
            $class   = $this->_components[$name]['class'];
            $options = isset($this->_components[$name]['options']) ? $this->_components[$name]['options']: null;

            if (!isset($this->_instantiatedComponents[$name])) {
                $this->_instantiatedComponents[$name] = new $class($options);
            }

            return $this->_instantiatedComponents[$name];
        }

        throw new Exception("Property \"$name\" not exists.");
    }

    /**
     * Метод инициализации приложения.
     */
    public function run()
    {
        register_shutdown_function([$this, 'end'], 0, false);
        set_exception_handler([$this, 'exceptionHandler']);
    }

    /**
     * Обработчик для неперехваченных исключений.
     * @param Exception $exception
     */
    public function exceptionHandler(Exception $exception)
    {
        restore_error_handler();
        restore_exception_handler();

        echo '<h1>' . get_class($exception) . "</h1>\n";
        echo '<p>' . $exception->getMessage() . ' (' . $exception->getFile() . ':' . $exception->getLine() . ')</p>';
        echo '<pre>' . $exception->getTraceAsString() . '</pre>';

        $this->end(1);
    }

    /**
     * Метод завершения приложения.
     * @param integer $status код завершения приложения.
     * @param integer $exit флаг завершения.
     */
    public function end($status = 0, $exit = true)
    {
        if ($exit) {
            exit($status);
        }
    }

}
