<?php

namespace core;

use Exception;

/**
 * Бащовый класс для контроллеров.
 *
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 * @package core
 * @uses controllers/SiteController.php
 */
class Controller
{

    /**
     * Наименование шаблона темы.
     * @var string
     */
    public $layout = 'main';

    /**
     * Директория размещения вьюшек.
     * @var string
     */
    public $viewPath = 'views';

    /** @var string */
    private $_id;

    /** @var string */
    private $_pageTitle;

    public function __construct()
    {
        $class     = strtolower(get_called_class());
        $class     = substr($class, (int) strrpos($class, '\\') + 1);
        $this->_id = str_replace('controller', '', $class);
    }

    /**
     * ID контроллера.
     * @return string
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Путь до файла вьюшки.
     * @param string $viewName наименование вьюшки.
     * @return string
     */
    protected function getViewFile($viewName)
    {
        $fileName = Fw::getBasePath() . DIRECTORY_SEPARATOR
                . $this->viewPath . DIRECTORY_SEPARATOR
                . $this->getId() . DIRECTORY_SEPARATOR
                . $viewName . '.php';

        return file_exists($fileName) ? $fileName : false;
    }

    /**
     * Путь до файла шаблона темы.
     * @param string $layoutName
     * @return string
     */
    protected function getLayoutFile($layoutName)
    {
        $fileName = Fw::getBasePath() . DIRECTORY_SEPARATOR
                . $this->viewPath . DIRECTORY_SEPARATOR
                . 'layouts' . DIRECTORY_SEPARATOR
                . $layoutName . '.php';

        return file_exists($fileName) ? $fileName : false;
    }

    /**
     * Отображение вьюшки вместе с щаблоном темы.
     * @param string $view наименование вьюшки.
     * @param array $data параметры.
     * @param boolean $return отобразить или вернуть строку.
     * @return string|void
     */
    public function render($view, $data = null, $return = false)
    {
        $output = $this->renderPartial($view, $data, true);

        if (($layoutFile = $this->getLayoutFile($this->layout)) !== false) {
            $output = $this->renderFile($layoutFile, ['content' => $output], true);
        }

        if ($return) {
            return $output;
        }

        echo $output;
    }

    /**
     * Отображение только вьюшки.
     * @param string $view наименование вьюшки.
     * @param array $data параметры.
     * @param boolean $return отобразить или вернуть строку.
     * @return string|void
     * @throws Exception если файл вьюшки не найден.
     */
    public function renderPartial($view, $data = null, $return = false)
    {
        if (($viewFile = $this->getViewFile($view)) !== false) {

            $output = $this->renderFile($viewFile, $data, true);
            if ($return) {
                return $output;
            } else {
                echo $output;
            }
        } else {
            throw new Exception("Cannot find the requested view \"$view\"");
        }
    }

    /**
     * Подгрузка файла шаблона.
     * @param string $_viewFile_ путь до файла шаблона.
     * @param array $_data_ параметры.
     * @param boolean $_return_ отобразить или вернуть строку.
     * @return string|void
     */
    protected function renderFile($_viewFile_, $_data_ = null, $_return_ = false)
    {
        // Параметры специально имеют такой вид, чтобы не возникло конфликта с параметрами шаблона.
        if (is_array($_data_)) {
            extract($_data_, EXTR_PREFIX_SAME, 'data');
        } else {
            $data = $_data_;
        }

        if ($_return_) {
            ob_start();
            ob_implicit_flush(false);
            require($_viewFile_);
            return ob_get_clean();
        }

        require($_viewFile_);
    }

    /**
     * Заголосов страницы.
     * Предварительно нужно задать методом $this->setPageTitle()
     * @return string
     */
    public function getPageTitle()
    {
        return ($this->_pageTitle ?: 'None title');
    }

    /**
     * Определение заголовка страницы.
     * @param string $value
     */
    public function setPageTitle($value)
    {
        $this->_pageTitle = $value;
    }

}
