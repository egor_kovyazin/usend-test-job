<?php

namespace core;

use Exception;

/**
 * Базовый класс фреймворка.
 * 
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 * @package core
 */
class Fw
{
    /** @var string */
    private static $_basePath;

    /** @var \core\Application */
    private static $_app;

    /**
     * Создание приложения.
     * @param string $className класс прилоежния.
     * @param array $config настройки.
     * @return \core\Application
     * @throws Exception если приложение уже создано.
     */
    protected static function createApplication($className, $config = null)
    {
        if (isset(self::$_app)) {
            throw new Exception('The application is already initialized.');
        }

        self::$_basePath = isset($config['base_path']) ? $config['base_path'] : __DIR__ . '/..';
        self::$_app      = new $className($config);

        return self::$_app;
    }

    /**
     * Создание web-прилоежения.
     * @param array $config настройки.
     * @return \core\WebApplication
     */
    public static function createWebApplication($config = null)
    {
        return self::createApplication('\core\WebApplication', $config);
    }

    /**
     * Ссылка на текущее прилоежние.
     * @return Application
     */
    public static function app()
    {
        return self::$_app;
    }

    /**
     * Путь до конрня проекта.
     * @return string
     */
    public static function getBasePath()
    {
        return self::$_basePath;
    }

}
