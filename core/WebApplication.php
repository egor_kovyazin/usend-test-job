<?php

namespace core;

/**
 * Web-приложение.
 *
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 * @package core
 */
class WebApplication extends Application
{

    /**
     * Контроллер по умолчанию.
     * @var string
     */
    public $defaultController = 'site';

    /**
     * Action по умолчанию во всех контроллеров.
     * @var string
     */
    public $defaultControllerAction = 'index';

    /**
     * Размещение контроллеров.
     * @var string
     */
    public $controllerNamespace = 'controllers';

    /**
     * Запуск действия.
     * @param array $route as [
     *   'controller' => <constroller name>,
     *   'action'     => <action name>
     * ]
     */
    public function runController($route)
    {
        $controllerId = !empty($route['controller']) ? $route['controller'] : $this->defaultController;
        $className    = $this->controllerNamespace . '\\' . ucfirst($controllerId) . 'Controller';

        $controller = new $className();
        $actionId   = 'action' . ucfirst(($route['action'] ?: $this->defaultControllerAction));

        $controller->$actionId();
    }

    /**
     * Запуск приложения.
     */
    public function run()
    {
        parent::run();
        $this->runController($this->parseUri());
    }

    /**
     * Разбор URI запроса.
     * @return array as [
     *   'controller' => <constroller name>,
     *   'action'     => <action name>
     * ]
     */
    protected function parseUri()
    {
        $parsed = parse_url($_SERVER['REQUEST_URI'])['path'];
        $parsed = trim($parsed, '/');
        $route  = explode('/', $parsed);

        return [
            'controller' => strpos($route[0], '&') !== false ? null : $route[0],
            'action'     => (isset($route[1]) ? $route[1] : null),
        ];
    }

}
