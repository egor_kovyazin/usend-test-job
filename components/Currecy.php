<?php

namespace components;

/**
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 */
class Currecy
{
    const RUB = 'RUB';
    const USD = 'USD';

    /**
     * @var self
     */
    private static $_instance;

    private function __construct()
    {
    }

    /**
     * @return Currecy
     */
    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * @param double $value amount in currency FROM
     * @param string $from  Char code
     * @param string $to    Char code
     * @param string $date  date as format YYYY-mm-dd HH:ii:ss
     * 
     * @return double
     */
    public function convert($value, $from, $to, $date = 'now')
    {
        return $value * rand(1, 3);
    }
}
