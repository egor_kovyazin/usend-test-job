<?php

namespace components;

use Exception;

/**
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 */
class Logger implements LoggerInterface
{
    /**
     * @var boolean
     */
    private $_isActive = true;

    /**
     * @var string
     */
    private $_logPath;

    /**
     * @var string
     */
    private $_logName = 'app.log';

    public function __construct(array $params = [])
    {
        $this->_isActive = isset($params['isActive']) ? $params['isActive']: $this->_isActive;
        $this->_logName  = isset($params['logName']) ? $params['logName']: $this->_logName;

        if (!isset($params['logPath'])) {
            throw new Exception('Argument "logPath" is required.');
        }

        $this->_logPath  = $params['logPath'];
    }

    /**
     * Write logged message
     * @param string $message
     */
    public function log(string $message)
    {
        if (!$this->_isActive) {
            return;
        }

        $fileName = $this->_logPath . DIRECTORY_SEPARATOR . $this->_logName;

        @file_put_contents($fileName, $message . PHP_EOL, FILE_APPEND);
    }

    /**
     * @return boolean
     */
    public function getIsActive(): bool
    {
        return $this->_isActive;
    }

    /**
     * @param boolean $state
     */
    public function setIsActive(bool $state)
    {
        $this->_isActive = $state;
    }

}
