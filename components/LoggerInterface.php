<?php

namespace components;

/**
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 */
interface LoggerInterface
{
    function log(string $message);
}
