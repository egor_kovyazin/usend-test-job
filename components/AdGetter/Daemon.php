<?php

namespace components\AdGetter;

use models\Ad;

/**
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 */
class Daemon extends Base
{
    protected function getGetterFunctionName(): string
    {
        return 'get_deamon_ad_info';
    }

    /**
     * @param integer $id
     * @return \models\Ad
     */
    protected function getPreparedData($id)
    {
        $data = explode("\t", get_deamon_ad_info($id));

        $ad = new Ad();

        $ad->id          = $data[0];
        $ad->campaign_id = $data[1];
        $ad->user_id     = $data[2];
        $ad->name        = $data[3];
        $ad->text        = $data[4];
        $ad->price       = $data[5];

        return $ad;
    }
}
