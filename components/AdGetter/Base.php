<?php

namespace components\AdGetter;

use components\LoggerInterface;

/**
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 */
abstract class Base
{
    /**
     * @var \components\LoggerInterface
     */
    private $_logger;

    /**
     * @param \components\ $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->_logger = $logger;
    }

    /**
     * @param integer $id
     * @return \models\Ad
     */
    public function getData($id)
    {
        $this->_logger->log(sprintf('[%s] %s(ID=%d)', date('Y-m-d H:i:s'), $this->getGetterFunctionName(), $id));

        return $this->getPreparedData($id);
    }

    abstract protected function getGetterFunctionName(): string;

    /**
     * @param integer $id
     * @return \models\Ad
     */
    abstract protected function getPreparedData($id);
}
