<?php

namespace components\AdGetter;

use models\Ad;

/**
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 */
class Mysql extends Base
{
    protected function getGetterFunctionName(): string
    {
        return 'getAdRecord';
    }

    /**
     * @param integer $id
     * @return \models\Ad
     */
    protected function getPreparedData($id)
    {
        $data = getAdRecord($id);

        $ad = new Ad();

        $ad->id    = $data['id'];
        $ad->name  = $data['name'];
        $ad->text  = $data['text'];
        $ad->price = $data['price'];

        $ad->setExtendedAttribute('keywords', $data['name']);

        return $ad;
    }
}
