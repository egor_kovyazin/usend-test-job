<?php
/* @var $this \core\Controller */
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="/css/site.css" />
        <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" />
        <title><?= 'Test job::' . htmlspecialchars($this->getPageTitle()) ?></title>
        <script src="/js/jquery.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
    </head>
    <body>

        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Test job</a>
                </div>
            </div>
        </nav>

        <div class="wrap">
            <div class="container">
                <?= $content ?>
            </div>
        </div>

        <footer class="footer">
            <div class="container">
                <p class="pull-left">&copy; Test job <?= date('Y') ?></p>
                <p class="pull-right">Powered by...</p>
            </div>
        </footer>

    </body>
</html>
