<?php
/**
 * @var \controllers\SiteController $this
 * @var \models\AdView $adData
 * @var array $errors
 * @var array $params
 */
$this->setPageTitle('главная');
?>

<?= $this->renderPartial('_selector', ['params' => $params]) ?>

<?php if (empty($errors)): ?>

<h1><?= $adData->name ?></h1>
<p><?= $adData->text ?></p>
<p>стоимость: <?= number_format($adData->price, 2) ?> руб</p>

<?php else: ?>

<div class="alert alert-warning" role="alert">
    <strong>Warning!</strong> <?= array_shift($errors) ?>
</div>

<?php endif; ?>
