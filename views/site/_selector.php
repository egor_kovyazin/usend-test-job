<?php
/** @var array $params */
?>

<nav class="navbar navbar-default">
    <form class="navbar-form navbar-left" role="data" method="get" action="/">
        <div class="form-group">
            <input type="number" class="form-control" name="id" placeholder="ID" value="<?= isset($params['id']) ? htmlspecialchars($params['id']): '' ?>">
            <select name="from">
                <option value="Mysql" <?php if ($params['from'] === 'Mysql'): ?>selected<?php endif; ?>>Mysql</option>
                <option value="Daemon" <?php if ($params['from'] === 'Daemon'): ?>selected<?php endif; ?>>Daemon</option>
            </select>
               
        </div>
        <button type="submit" class="btn btn-default">Select</button>
    </form>
</nav>
