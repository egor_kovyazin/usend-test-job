<?php

/**
 * Тестовый конфигурационный файл приложения.
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 * @uses test/unit/_bootstrap.php
 */
$rootDir = isset($rootDir) ? $rootDir : __DIR__ . '/../..';

return [
    // Путь до корня проекта.
    'basePath' => $rootDir,
    'components' => [],
    'params' => [
    ],
];

