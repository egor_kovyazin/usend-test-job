<?php

namespace controllers;

use core\Controller;
use core\Fw;
use models\AdView;

/**
 * Контроллер "site"
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 */
class SiteController extends Controller
{

    const AD_GETTERS_NAMESPACE = '\components\AdGetter\\';

    public function actionIndex()
    {
        $params['id']   = !empty($_GET['id']) ? (int)$_GET['id']: null;
        $params['from'] = !empty($_GET['from']) ? $_GET['from']: '';

        $params['from'] = str_replace(['/', '\\', ' ', '.'], '', ucfirst(strtolower($params['from'])));

        $adGetterClass = self::AD_GETTERS_NAMESPACE . $params['from'];

        if (class_exists($adGetterClass) && !is_null($params['id'])) {
            $adGetter = new $adGetterClass(Fw::app()->logger);
            $adData = $adGetter->getData($params['id']);

            $this->render('index', [
                'adData' => new AdView($adData),
                'params' => $params,
            ]);
            
        }

        $this->render('index', [
            'errors' => ['Request params are failed.'],
            'params' => [],
        ]);
    }

}
