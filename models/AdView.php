<?php

namespace models;

use components\Currecy;

/**
 * Readonly model for Ads.
 * 
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 * 
 * @property integer $id
 * @property integer $campaign_id
 * @property integer $user_id
 * @property string  $name
 * @property string  $text
 * @property double  $price
 */
class AdView extends Ad
{
    public function __construct(Ad $ad)
    {
        $this->_attributes = $ad->getAttributes();
    }

    /**
     * {@inheritdoc}
     */
    public function __set($name, $value)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function setExtendedAttribute($name, $value)
    {
    }

    /**
     * @return double
     */
    public function price()
    {
        return Currecy::getInstance()->convert($this->_attributes['price'], 'USD', 'RUB') ;
    }
}
