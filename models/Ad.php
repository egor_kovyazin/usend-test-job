<?php

namespace models;

use Exception;

/**
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 * 
 * @property integer $id
 * @property integer $campaign_id
 * @property integer $user_id
 * @property string  $name
 * @property string  $text
 * @property double  $price
 */
class Ad
{
    protected $_attributes = [
        'id'          => null,
        'campaign_id' => null,
        'user_id'     => null,
        'name'        => null,
        'text'        => null,
        'price'       => null,
    ];

    protected $_extendedAttributes = [];

    /**
     * @param string $name
     * 
     * @return mixed
     * @throws Exception
     */
    public function __get($name)
    {
        if (method_exists($this, $name)) {
            return $this->{$name}();
        } elseif (array_key_exists($name, $this->_attributes)) {
            return $this->_attributes[$name];
        }

        throw new Exception(sprintf('Property "%s" not exists.', $name));
    }

    /**
     * @param string $name
     * @param mixed $value
     * 
     * @throws Exception
     */
    public function __set($name, $value)
    {
        if (array_key_exists($name, $this->_attributes)) {
            $this->_attributes[$name] = $value;
        } else {
            throw new Exception(sprintf('Property "%s" not exists.', $name));
        }
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return $this->_attributes;
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function setExtendedAttribute($name, $value)
    {
        $this->_extendedAttributes[$name] = $value;
    }

    /**
     * @param string $name
     * 
     * @return mixed
     */
    public function getExtendedAttribute($name)
    {
        if (array_key_exists($name, $this->_extendedAttributes)) {
            return $this->_extendedAttributes[$name];
        }

        throw new Exception(sprintf('Extended property "%s" not exists.', $name));
    }
}
