<?php

/**
 * Основной конфигурационный файл приложения.
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 * @uses web/index.php | fwc
 */
$rootDir = isset($rootDir) ? $rootDir : __DIR__ . '/..';

return [
    // Путь до корня проекта.
    'basePath'   => $rootDir,
    'components' => [
        'logger' => [
            'class'   => \components\Logger::class,
            'options' => [
                'isActive' => true,
                'logPath' => $rootDir . DIRECTORY_SEPARATOR . 'runtime',
            ],
        ],

    ],
    'params' => [
        'fileManager' => [
            'dir' => $rootDir . '/components',
        ],
    ],
];

