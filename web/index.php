<?php
error_reporting(E_ALL & ~E_NOTICE);

$rootDir = __DIR__ . '/..';

require_once($rootDir . '/src/functions.php');

require_once($rootDir . '/core/autoload.php');

// Загрузка ядря
require_once($rootDir . '/core/Fw.php');

// Загрузка конфига
$config = require_once($rootDir . '/config/main.php');

// Инициализация web-приложения
$app = \core\Fw::createWebApplication($config);

// Запуск приложения
$app->run();
